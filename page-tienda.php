<?php
    get_header();
?>

<div class="jumbotron jumbo-tienda">
    <h1>Visita les nostres seccions</h1>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <img src="<?=get_theme_file_uri("inc/img/samarretes.jpg")?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Camisetas a medida</h5>
                    <a href="<?=site_url( "samarretes")?>" class="btn btn-primary">Ir...</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <img src="<?=get_theme_file_uri("inc/img/jeans.jpg")?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Jeans a medida</h5>
                    <a href="<?=site_url( "texans")?>" class="btn btn-primary">Ir...</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    get_footer();
?>