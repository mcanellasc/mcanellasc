<?php
    get_header();
?>

   <div class="jumbotron jumbo-jeans">
       <h1>Prueba nuestros jeans flexibles</h1>
   </div>

   <div class="container">
    <div class="row">
        <div class="col-12 col-md-3">
            <div class="card-img-top">
                <img src="<?=get_theme_file_uri("inc/img/jeans1.jpg")?>">
                <p>1981 - 19,90 €</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-img-top">
                <img src="<?=get_theme_file_uri("inc/img/jeans2.jpg")?>">
                <p>1981 - 19,90 €</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-img-top">
                <img src="<?=get_theme_file_uri("inc/img/jeans3.jpg")?>">
                <p>1981 - 19,90 €</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-img-top">
                <img src="<?=get_theme_file_uri("inc/img/jeans4.jpg")?>">
                <p>1981 - 19,90 €</p>
            </div>
        </div>
    </div>
</div>
<?php
    get_footer();
?>